# About

This is an implementation of Neural Style Transfer with a second style done with TensorFlow, Flask, ASP.NET Core, Angular and wrapped with Docker Compose.

The detailed description is available at the corresponding blog post [Dual Neural Style Transfer with Docker Compose](https://evgeniymamchenko.com/dual-neural-style-transfer-with-docker-compose/).

# Table of contents


* [Running the application](#running-the-application)

    * [Docker Compose](#docker-compose)

        * [Getting compose files](#getting-compose-files)

        * [On CPU](#on-cpu)

        * [On GPU](#on-gpu)

        * [How to change the exposed port of the UI container](#how-to-change-the-exposed-port-of-the-ui-container)

        * [How to limit the CPU usage](#how-to-limit-the-cpu-usage)

            * [cpus](#cpus)

            * [cpu_shares](#cpu_shares)

    * [Command line](#command-line)

        * [Python back-end](#python-back-end)

            * [CPU prerequisites](#cpu-prerequisites)

            * [GPU prerequisites](#gpu-prerequisites)

            * [Running the application](#running-the-application)

        * [ASP.NET Core / Angular front-end](#aspnet-core-angular-front-end)

    * [Visual Studio](#visual-studio)

# Running the application

## Docker Compose

### Getting compose files

When running with Docker Compose you do not have to clone the entire 500 MB repository (it contains the weights of pre-trained VGG network).
Alternatively, you can just download the required compose files:

```shell
curl https://gitlab.com/OutSorcerer/NeuralStyleTransferWeb/raw/master/docker-compose.yml > docker-compose.yml
curl https://gitlab.com/OutSorcerer/NeuralStyleTransferWeb/raw/master/docker-compose.prod.yml > docker-compose.prod.yml
```

### On CPU

```shell
docker-compose -f docker-compose.prod.yml pull && docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
```

### On GPU

First, you need to install [NVIDIA Container Runtime for Docker](https://github.com/NVIDIA/nvidia-docker). Follow the instructions from their README.md.

NVIDIA Container Runtime has a dependency on the NVIDIA GPU driver.
You can install it either [via a package manager like `apt-get`](https://gist.github.com/wangruohui/df039f0dc434d6486f5d4d098aa52d07#install-nvidia-graphics-driver-via-apt-get), either [via runfile](https://gist.github.com/wangruohui/df039f0dc434d6486f5d4d098aa52d07#install-nvidia-graphics-driver-via-runfile).
The latter option is recommended, as the most recent version is not usually available via a package manager.

For Docker Compose to use the NVIDIA runtime [add the setting](https://github.com/eywalker/nvidia-docker-compose/issues/23#issuecomment-342921564) `"default-runtime": "nvidia"` to your `/etc/docker/daemon.json`. 

After that, just launch the following command:

```shell
docker-compose -f docker-compose.prod.gpu.yml pull && docker-compose -f docker-compose.yml -f docker-compose.prod.gpu.yml up -d
```

### How to change the exposed port of the UI container

You might need to deploy it on a port other but default 80. To do it simply set the environment variable `NST_UI_EXTERNAL_PORT`, for example:

```shell
export NST_UI_EXTERNAL_PORT=51285
docker-compose ...
```

### How to limit the CPU usage

The need for this [is explained here](https://evgeniymamchenko.com/dual-neural-style-transfer-with-docker-compose/#long-response-times).

For stability of the whole app CPU usage of the back-end must be limited, as TensorFlow occupies 100% of CPU by default.

For that purpose add `docker-compose.prod.override.yml` file (is already added to `.gitignore`) with the following content:

```yaml
version: '2.3'

services:
  neural-style-transfer-ui:
    cpu_shares: 4096

  neural-style-transfer-backend:
    cpus: 1.9
    cpu_shares: 1024
```

When  that file with `-f` command line parameter, for example:

```shell
docker-compose -f docker-compose.prod.yml pull && docker-compose -f docker-compose.yml -f docker-compose.prod.yml -f docker-compose.prod.override.yml up -d
```

Note: Version 2.3. is not obsolete and appeared approximately [at the same time as 3.4](https://github.com/docker/compose/issues/4513).

#### cpus

It sets [a limit of CPU cores](https://docs.docker.com/config/containers/resource_constraints/#configure-the-default-cfs-scheduler) that a specific service can use.

#### cpu_shares

It sets [a weight](https://docs.docker.com/config/containers/resource_constraints/#configure-the-default-cfs-scheduler) that determines which proportion of CPU each service receives. Only takes effect when CPU resources are constrained.

## Command line

### Python back-end

Requires [Python 3](https://www.python.org/). TensorFlow [requires Python 3.4+](https://www.tensorflow.org/install/install_linux#prerequisite_python_and_pip).
This application was tested with Python 3.5.2.

First, from the root of the repository go to `NeuralStyleTransferBackend` folder.

#### CPU prerequisites

Install the required packages with:

```python
pip install --requirement requirements.txt
```

#### GPU prerequisites

Additionally to CPU prerequisites do:

```python
pip install tensorflow==1.8.0
```

See [the detailed TensorFlow installation instructions](https://www.tensorflow.org/install/) in case of issues.

#### Running the application

Run the application with:

```
python NeuralStyleTransferBackend.py
```

You should see something like:


```
* Running on http://0.0.0.0:51286/ (Press CTRL+C to quit)
...
Model weights have been loaded.
Computation graph has been initialized.
```

If application is using GPU you should see something like:

```
Found device 0 with properties:
name: GeForce GTX 1050 major: 6 minor: 1 memoryClockRate(GHz): 1.493
pciBusID: 0000:01:00.0
totalMemory: 2.00GiB freeMemory: 1.61GiB
```

To listen for another port, change it by setting `NST_BACKEND_PORT` environment variable (the default value is `51286`). 

### ASP.NET Core / Angular front-end

Requires [.NET Core SDK 2.0+](https://www.microsoft.com/net/download/). 

From the root of the repository go to `NeuralStyleTransferUI` folder.

Launch the application with the .NET Core CLI:

```
dotnet run
```

Starting with .NET Core 2.0, you don't have to run `dotnet restore` because it's run implicitly by all commands that require a restore like `dotnet run`.

If the ASP.NET Core component starts successfully you should see:

```
Now listening on: http://localhost:51285
```

Open the address above in your browser and start using the application.

To use it with another back-end URL change the value of `NST_BACKEND__URL` environment variable.

## Visual Studio

Just open the solution file, right-click the solution in the Solution Explorer window, select "Multiple startup projects" and
set action "Start" both for `NeuralStyleTransferBackend` and `NeuralStyleTransferUI`.

Select an appropriate Python environment. Open `NeuralStyleTransferBackend` project properties and on the `General` tab chose an `Interpreter`.

You may also need to install missing Python packages, see [CPU prerequisites](#cpu-prerequisites) and [GPU prerequisites](#gpu-prerequisites) above.

Press "Start Debugging" (F5).
