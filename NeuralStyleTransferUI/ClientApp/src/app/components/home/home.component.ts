import { Component, Inject } from "@angular/core";
import { Subscription } from "rxjs/Subscription";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/timer';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/catch';
import { HttpClient } from "@angular/common/http";
import { AbstractControl, FormGroup, FormControl, FormBuilder } from "@angular/forms";

const placeholder = String(require("../../../assets/placeholder.png"));

@Component({
  selector: "home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent {
  formGroup: FormGroup;
  contentImageControl: FormControl;
  styleImageControl: FormControl;
  style2ImageControl: FormControl;
  status: string = "";
  currentJob: any = null;
  pollingSubscriber: Subscription | null;
  running = false;
  timeElapsed: Observable<string> | null;

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    @Inject("BASE_URL") private baseUrl: string) {
    this.initForm();
  }

  contentImageChanged(event: any) {
    const currentFile = event.target.files[0];
    event.target.value = null;
    const reader = new FileReader();
    reader.onload = () => {
      this.contentImageControl.setValue(reader.result);
      this.contentImageControl.markAsDirty();
    };
    reader.readAsDataURL(currentFile);
  }

  contentImageRemoved() {
    this.contentImageControl.reset(placeholder);
  }

  styleImageChanged(event: any) {
    const currentFile = event.target.files[0];
    event.target.value = null;
    const reader = new FileReader();
    reader.onload = () => {
      this.styleImageControl.setValue(reader.result);
      this.styleImageControl.markAsDirty();
    };
    reader.readAsDataURL(currentFile);
  }

  styleImageRemoved() {
    this.styleImageControl.reset(placeholder);
    this.style2ImageControl.reset(placeholder);
  }

  style2ImageChanged(event: any) {
    const currentFile = event.target.files[0];
    event.target.value = null;
    const reader = new FileReader();
    reader.onload = () => {
      this.style2ImageControl.setValue(reader.result);
      this.style2ImageControl.markAsDirty();
    };
    reader.readAsDataURL(currentFile);
  }

  style2ImageRemoved() {
    this.style2ImageControl.reset(placeholder);
  }

  async startTransfer() {
    this.currentJob = {
      initializing: true,
      iteration: 0,
      totalCost: 0,
      contentCost: 0,
      styleCost: 0
    };
    this.running = true;
    const startTime = new Date();
    this.timeElapsed = Observable.timer(0, 100)
      .map(() => new Date().getTime() - startTime.getTime())
      .map(elapsed => HomeComponent.formatTimeSpan(elapsed))
      .takeWhile(() => this.running);
    try {
      const createJobResponseData = await this.http.put<any>(
          this.baseUrl + "api/neural-style-transfer/jobs",
          this.formGroup.value)
        .toPromise();
      const jobId = String(createJobResponseData["jobId"]);
      const polling = Observable.timer(0, 1000)
        .switchMap(() => this.http.get<any>(this.baseUrl + `api/neural-style-transfer/jobs/${jobId}`))
        .catch((e: any) => {
          return this.errorHandler(e);
        });
      this.pollingSubscriber = polling.subscribe(job => {
        const previousImage = this.currentJob && this.currentJob.resultImage;
        this.currentJob = job;
        if (job.resultImage === null) {
          this.currentJob.resultImage = previousImage;
        }
        if (job.completed || job.failed || job.cancelled) {
          this.running = false;
          if (this.pollingSubscriber) {
            this.pollingSubscriber.unsubscribe();
            this.pollingSubscriber = null;
          }
          return;
        }
      });
    }
    catch (e) {
      this.running = false;
      if (this.pollingSubscriber) {
        this.pollingSubscriber.unsubscribe();
        this.pollingSubscriber = null;
      }
      this.currentJob.initializing = false;
      this.currentJob.failed = true;
    }
  }

  stopTransfer() {
    this.currentJob.cancelled = true;
    this.running = false;
    if (this.pollingSubscriber) {
      this.pollingSubscriber.unsubscribe();
      this.pollingSubscriber = null;
    }
    this.http.delete(this.baseUrl + `api/neural-style-transfer/jobs/${this.currentJob.id}`)
      .subscribe();
  }

  imageRequiredValidator(control: AbstractControl) {
    if (!control.value || control.value === placeholder) {
      return { imageRequired: true };
    }
    return null;
  }

  initForm(): any {
    this.contentImageControl = new FormControl(placeholder, [this.imageRequiredValidator]);
    this.styleImageControl = new FormControl(placeholder, [this.imageRequiredValidator]);
    this.style2ImageControl = new FormControl(placeholder);
    this.formGroup = new FormGroup({
      contentImage: this.contentImageControl,
      styleImage: this.styleImageControl,
      style2Image: this.style2ImageControl
    });
  }

  get transferEnabled(): boolean {
    return !this.running && !this.formGroup.pristine && !this.formGroup.invalid;
  }

  get stopEnabled(): boolean {
    return this.running;
  }

  static formatTimeSpan(totalElapsedMilliseconds: number): string {
    const pad = function (value: number, size: number) {
      var s = new String(value);
      while (s.length < size) {
        s = "0" + s;
      }
      return s;
    };
    const elapsedMilliseconds = totalElapsedMilliseconds % 1000;
    const totalElapsedSeconds = Math.trunc(totalElapsedMilliseconds / 1000);
    const elapsedSeconds = totalElapsedSeconds % 60;
    const totalElapsedMinutes = Math.trunc(totalElapsedSeconds / 60);
    const elapsedMinutes = totalElapsedMinutes % 60;
    const totalElapsedHours = Math.trunc(totalElapsedMinutes / 60);
    return `${pad(totalElapsedHours,2)}:${pad(elapsedMinutes,2)}:${pad(elapsedSeconds, 2)}.${pad(elapsedMilliseconds, 3)}`;
  };
  
  get generatedImage(): boolean {
    return this.currentJob && this.currentJob.resultImage ? this.currentJob.resultImage : placeholder;
  }

  errorHandler(error: any): Observable<any> {
    this.running = false;
    this.currentJob.failed = true;
    return Observable.throw(error);
  }
}
