import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Component({
  selector: "parameters",
  templateUrl: "./parameters.component.html",
  styleUrls: ["./parameters.component.css"]
})
export class ParametersComponent implements OnInit {
  readonly positiveIntegerNumberRegex = /^\d+$/;
  readonly positiveRealNumberRegex = /^\d+(\.\d+)?$/;

  @Input() parent: FormGroup;

  private _disabled : Boolean;
  @Input()
  set disabled(value: Boolean) {
    this._disabled = value;
    if (this._disabled) {
      this.parametersGroup && this.parametersGroup.disable();
    } else {
      this.parametersGroup && this.parametersGroup.enable();
    }
  }
  get disabled() {
    return this._disabled;
  }

  parametersGroup: FormGroup;
  iterations: FormControl;
  contentCostWeight: FormControl;
  styleCostWeight: FormControl;

  ngOnInit() {
    this.createForm();
  }
  
  createForm() {
    this.iterations = new FormControl(100, [Validators.required, Validators.pattern(this.positiveIntegerNumberRegex), Validators.max(1000)]);
    this.contentCostWeight = new FormControl(10.0, [Validators.required, Validators.pattern(this.positiveRealNumberRegex)]);
    this.styleCostWeight = new FormControl(40.0, [Validators.required, Validators.pattern(this.positiveRealNumberRegex)]);    
    this.parametersGroup = new FormGroup({
      iterations: this.iterations,
      contentCostWeight: this.contentCostWeight,
      styleCostWeight: this.styleCostWeight
    });
    this.parent.addControl('parameters', this.parametersGroup);
  }
}
