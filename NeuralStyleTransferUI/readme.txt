# How to build and push UI image

From solution root folder:

docker build -f ./NeuralStyleTransferUI/Dockerfile . --target "final" --tag neural-style-transfer-ui
docker tag neural-style-transfer-ui sourcerer/neural-style-transfer-ui
docker push sourcerer/neural-style-transfer-ui



