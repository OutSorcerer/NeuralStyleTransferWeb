using System;

namespace NeuralStyleTransferUI.Models
{
    public class BackendJobModel
    {
        public Guid Id { get; set; }

        public string ContentImage { get; set; }

        public string StyleImage { get; set; }

        public string Style2Image { get; set; }

        public int Iterations { get; set; }

        public double ContentCostWeight { get; set; }

        public double StyleCostWeight { get; set; }
    }
}
