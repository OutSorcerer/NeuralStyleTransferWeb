using System.ComponentModel.DataAnnotations;

namespace NeuralStyleTransferUI.Models
{
    public class JobModel
    {
        [Required]
        public string ContentImage { get; set; }

        [Required]
        public string StyleImage { get; set; }

        public string Style2Image { get; set; }

        public JobParametersModel Parameters { get; set; }
    }
}
