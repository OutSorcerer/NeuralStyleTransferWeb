using System;

namespace NeuralStyleTransferUI.Models
{
    public class JobViewModel
    {
        public Guid Id { get; set; }

        public bool Queued { get; set; }

        public bool Initializing { get; set; }

        public bool Cancelled { get; set; }

        public bool Completed { get; set; }

        public bool Failed { get; set; }

        public string ResultImage { get; set; }

        public int Iteration { get; set; } = 0;

        public double TotalCost { get; set; }

        public double ContentCost { get; set; }

        public double StyleCost { get; set; }
    }
}
