namespace NeuralStyleTransferUI.Models
{
    public class JobParametersModel
    {
        public int Iterations { get; set; }

        public double ContentCostWeight { get; set; }

        public double StyleCostWeight { get; set; }
    }
}
