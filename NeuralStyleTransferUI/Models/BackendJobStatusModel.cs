namespace NeuralStyleTransferUI.Models
{
    public class BackendJobStatusModel
    {
        public string ResultImage { get; set; }

        public int Iteration { get; set; }

        public double TotalCost { get; set; }

        public double ContentCost { get; set; }

        public double StyleCost { get; set; }

        public bool Initializing { get; set; }

        public bool Failed { get; set; }
    }
}
