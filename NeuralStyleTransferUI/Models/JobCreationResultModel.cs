namespace NeuralStyleTransferUI.Models
{
    public class JobCreationResultModel
    {
        public bool Created { get; set; }
    }
}
