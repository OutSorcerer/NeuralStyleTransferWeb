using System;
using System.Threading;
using SixLabors.ImageSharp;

namespace NeuralStyleTransferUI.Models
{
    public class Job
    {
        public Guid Id { get; set; }

        public Image<Rgba32> ContentImage { get; set; }

        public Image<Rgba32> StyleImage { get; set; }

        public Image<Rgba32> Style2Image { get; set; }

        public bool Queued { get; set; }

        public bool Initializing { get; set; }

        public bool Cancelled { get; set; }

        public bool Completed { get; set; }

        public bool Failed { get; set; }

        private bool newImage = false;
        private string lastResultImage = null;
        private string resultImage = null;
        public string ResultImage {
            get
            {
                return resultImage;
            }
            set
            {
                resultImage = value;
                if (lastResultImage == null || resultImage != lastResultImage)
                {
                    lastResultImage = resultImage;
                    newImage = true;
                }
            }
        }

        // Only returns each image once to prevent excessive HTTP traffic.
        public string NewResultImage
        {
            get
            {
                var result = newImage ? resultImage : null;
                newImage = false;
                return result;
            }
        }

        public int Iteration { get; set; } = 0;

        public double TotalCost { get; set; }

        public double ContentCost { get; set; }

        public double StyleCost { get; set; }

        public JobParametersModel Parameters { get; set; }

        public CancellationTokenSource CancellationTokenSource { get; set; }

        public DateTime LastQueriedAt { get; set; } = DateTime.Now;

        public JobViewModel ToViewModel()
        {
            return new JobViewModel
            {
                Id = Id,
                Cancelled = Cancelled,
                Completed = Completed,
                Queued = Queued,
                Failed = Failed,
                ContentCost = ContentCost,
                Iteration = Iteration,
                ResultImage = NewResultImage,
                StyleCost = StyleCost,
                TotalCost = TotalCost,
                Initializing = Initializing
            };
        }
    }
}
