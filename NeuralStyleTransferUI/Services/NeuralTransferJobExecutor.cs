using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NeuralStyleTransferUI.Controllers;
using NeuralStyleTransferUI.Models;
using Newtonsoft.Json;
using SixLabors.ImageSharp;

namespace NeuralStyleTransferUI.Services
{
    public class NeuralTransferJobExecutor
    {
        private const int ImageWidth = 400;
        private const int ImageHeight = 300;
        private readonly Regex _totalCostRegex = new Regex(@"total cost = (?<cost>\d*.\d*)");
        private readonly Regex _contentCostRegex = new Regex(@"content cost = (?<cost>\d*.\d*)");
        private readonly Regex _styleCostRegex = new Regex(@"style cost = (?<cost>\d*.\d*)");

        private readonly BufferBlock<Job> _waitingJobs = new BufferBlock<Job>();
        private readonly List<Job> _jobs = new List<Job>();
        private readonly ILogger<NeuralTransferController> _logger;
        private readonly IConfiguration _configuration;
        private readonly IOptions<MvcJsonOptions> _jsonOptions;
        private readonly HttpClient _client = new HttpClient() { Timeout = TimeSpan.FromSeconds(60) };
        private readonly IOptions<BackendOptions> _backendOptions;
        private readonly TimeSpan _inactivityTimeout;

        public NeuralTransferJobExecutor(ILogger<NeuralTransferController> logger, IConfiguration configuration,
            IOptions<MvcJsonOptions> jsonOptions, IOptions<BackendOptions> backendOptions)
        {
            _logger = logger;
            _configuration = configuration;
            _inactivityTimeout = TimeSpan.FromSeconds(_configuration.GetValue<int>("INACTIVITY_TIMEOUT_SEC", 60));
            _jsonOptions = jsonOptions;
            _backendOptions = backendOptions;
            Task.Run(async () =>
            {
                while (await _waitingJobs.OutputAvailableAsync())
                {
                    var job = await _waitingJobs.ReceiveAsync();
                    if (!job.Cancelled)
                    {
                        job.Queued = false;
                        job.Initializing = true;
                        job.CancellationTokenSource = new CancellationTokenSource();
                        try
                        {
                            await ExecuteJob(job, job.CancellationTokenSource.Token);
                        }
                        catch
                        {
                            job.Initializing = false;
                            job.Failed = true;
                        }
                    }
                    job.Completed = !job.Cancelled && !job.Failed;
                }
            });
        }

        public Job StartJob(JobModel model)
        {
            // Resize images https://andrewlock.net/using-imagesharp-to-resize-images-in-asp-net-core-a-comparison-with-corecompat-system-drawing/
            var newJob = new Job
            {
                Id = Guid.NewGuid(),
                ContentImage = Resize(FromBase64(model.ContentImage)),
                StyleImage = Resize(FromBase64(model.StyleImage)),
                Style2Image = Resize(FromBase64(model.Style2Image)),
                Parameters = model.Parameters,
                Queued = true
            };
            _jobs.Add(newJob);
            _waitingJobs.Post(newJob);
            return newJob;
        }

        public void StopJob(Job job)
        {
            if (job.Queued)
            {
            }
            else if (!job.Queued && !job.Cancelled)
            {
                job.CancellationTokenSource.Cancel();
            }
            job.Initializing = false;
            job.Cancelled = true;
        }

        public Job GetJob(Guid jobId)
        {
            var job = _jobs.FirstOrDefault(j => j.Id == jobId);
            if (job != null)
            {
                job.LastQueriedAt = DateTime.Now;
            }
            return job;
        }

        private Image<Rgba32> Resize(Image<Rgba32> image)
        {
            if (image == null)
            {
                return null;
            }
            var result = image.Clone();
            result.Mutate(context => context.Resize(ImageWidth, ImageHeight));
            return result;
        }

        private Image<Rgba32> FromBase64(string base64ContentUrl)
        {
            var match = new Regex("data:.+;base64,(?<base64content>.+)").Match(base64ContentUrl);
            if (!match.Success)
            {
                return null;
            }
            var base64Content = match.Groups["base64content"].Value;
            return Image.Load(Convert.FromBase64String(base64Content));
        }
        
        private async Task ExecuteJob(Job job, CancellationToken cancellationToken)
        {
            var jobJson = JsonConvert.SerializeObject(
                new BackendJobModel
                {
                    Id = job.Id,
                    ContentImage = job.ContentImage.ToBase64String(ImageFormats.Png),
                    StyleImage = job.StyleImage.ToBase64String(ImageFormats.Png),
                    Style2Image = job.Style2Image?.ToBase64String(ImageFormats.Png),
                    Iterations = job.Parameters.Iterations,
                    ContentCostWeight = job.Parameters.ContentCostWeight,
                    StyleCostWeight = job.Parameters.StyleCostWeight
                }, _jsonOptions.Value.SerializerSettings);
            var jobCreationResponse = await _client.PutAsync($"{_backendOptions.Value.Url}/api/neural-style-transfer-backend/job",
                new StringContent(jobJson, Encoding.UTF8, "application/json"));
            jobCreationResponse.EnsureSuccessStatusCode();
            var jobCreationResponseContent = await jobCreationResponse.Content.ReadAsStringAsync();
            var jobCreationResult = JsonConvert.DeserializeObject<JobCreationResultModel>(jobCreationResponseContent);
            if (!jobCreationResult.Created)
            {
                throw new Exception();
            }
            cancellationToken.Register(async () =>
            {
                var jobDeletionResponse = await _client.DeleteAsync($"{_backendOptions.Value.Url}/api/neural-style-transfer-backend/job/{job.Id}");
                jobDeletionResponse.EnsureSuccessStatusCode();
            });
            while (!cancellationToken.IsCancellationRequested && job.Iteration < job.Parameters.Iterations - 1 && !job.Failed)
            {
                if (DateTime.Now - job.LastQueriedAt > _inactivityTimeout)
                {
                    // The front-end should query status per seconds, so if 10 seconds elapsed the job should be cancelled.
                    job.Cancelled = true;
                    var jobDeletionResponse = await _client.DeleteAsync($"{_backendOptions.Value.Url}/api/neural-style-transfer-backend/job/{job.Id}");
                    jobDeletionResponse.EnsureSuccessStatusCode();
                    return;
                }
                var jobStatusResponse = await _client.GetAsync($"{_backendOptions.Value.Url}/api/neural-style-transfer-backend/job/{job.Id}");
                jobStatusResponse.EnsureSuccessStatusCode();
                var jobStatusReponseContent = await jobStatusResponse.Content.ReadAsStringAsync();
                var backendJobStatusModel = JsonConvert.DeserializeObject<BackendJobStatusModel>(jobStatusReponseContent, _jsonOptions.Value.SerializerSettings);
                job.Initializing = backendJobStatusModel.Initializing;
                job.Failed = backendJobStatusModel.Failed;
                job.ResultImage = backendJobStatusModel.ResultImage;
                job.Iteration = backendJobStatusModel.Iteration;
                job.TotalCost = backendJobStatusModel.TotalCost;
                job.ContentCost = backendJobStatusModel.ContentCost;
                job.StyleCost = backendJobStatusModel.StyleCost;
                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }
    }
}
