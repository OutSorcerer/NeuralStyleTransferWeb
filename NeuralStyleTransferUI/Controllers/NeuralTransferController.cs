using System;
using Microsoft.AspNetCore.Mvc;
using NeuralStyleTransferUI.Models;
using NeuralStyleTransferUI.Services;

namespace NeuralStyleTransferUI.Controllers
{
    [Route("api/neural-style-transfer")]
    public partial class NeuralTransferController : Controller
    {
        private readonly NeuralTransferJobExecutor _jobExecutor;

        public NeuralTransferController(NeuralTransferJobExecutor jobExecutor)
        {
            _jobExecutor = jobExecutor;
        }

        [HttpPut("jobs")]
        public ActionResult StartJob([FromBody] JobModel model)
        {
            if (!ModelState.IsValid || model == null)
            {
                return BadRequest();
            }
            var newJob = _jobExecutor.StartJob(model);
            return Ok(new { jobId = newJob.Id });
        }

        [HttpDelete("jobs/{jobId:guid}")]
        public ActionResult StopJob(Guid jobId)
        {
            var job = _jobExecutor.GetJob(jobId);
            if (job == null)
            {
                return NotFound();
            }
            _jobExecutor.StopJob(job);
            return Ok();
        }

        [HttpGet("jobs/{jobId:guid}")]
        public ActionResult GetJob(Guid jobId)
        {
            var job = _jobExecutor.GetJob(jobId);
            if (job == null)
            {
                return NotFound();
            }
            return Ok(job.ToViewModel());
        }
    }
}
