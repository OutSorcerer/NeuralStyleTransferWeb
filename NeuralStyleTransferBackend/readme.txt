# How to build and push backend image

docker build . --tag neural-style-transfer-backend:latest
docker tag neural-style-transfer-backend:latest sourcerer/neural-style-transfer-backend:latest
docker push sourcerer/neural-style-transfer-backend:latest

# How to build and push GPU backend image

docker build -f Dockerfile.gpu . --tag neural-style-transfer-backend:latest-gpu
docker tag neural-style-transfer-backend:latest-gpu sourcerer/neural-style-transfer-backend:latest-gpu
docker push sourcerer/neural-style-transfer-backend:latest-gpu

# How to build and push backend image (no AVX)

docker build -f Dockerfile.no-avx . --tag neural-style-transfer-backend:no-avx
docker tag neural-style-transfer-backend:no-avx sourcerer/neural-style-transfer-backend:no-avx
docker push sourcerer/neural-style-transfer-backend:no-avx

# How to launch back-end

docker run -it -e "NST_BACKEND_PORT=51286" -p "51286:51286" neural-style-transfer-backend
