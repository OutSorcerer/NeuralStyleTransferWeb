class BackendJobStatusModel():
    def __init__(self):
        self.resultImage = None
        self.iteration = 0
        self.totalCost = 0.0
        self.contentCost = 0.0
        self.styleCost = 0.0
        self.completed = False
        self.cancelled = False
        self.initializing = True

    def serialize(self):
        return self.__dict__
