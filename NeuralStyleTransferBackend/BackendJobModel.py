class BackendJobModel():
    def __init__(self, **entries):
        self.id = None
        self.contentImage = ''
        self.styleImage = ''
        self.iterations = 0
        self.contentCostWeight = 0.0
        self.styleCostWeight = 0.0
        self.__dict__.update(entries)

    def serialize(self):
        return self.__dict__
