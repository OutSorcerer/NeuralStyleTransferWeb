# This code is based on an assignment Art Generation with Neural Style Transfer from Andrew's Ng course on Coursera: https://www.coursera.org/learn/convolutional-neural-networks,
# which is a part of Deep Learning specialization https://www.deeplearning.ai/.
#
# References:
# The Neural Style Transfer algorithm was due to Gatys et al. (2015).
# Harish Narayanan and Github user "log0" also have highly readable write-ups from which we drew inspiration.
# The pre-trained network used in this implementation is a VGG network, which is due to Simonyan and Zisserman (2015).
# Pre-trained weights were from the work of the MathConvNet team.
#
# Leon A. Gatys, Alexander S. Ecker, Matthias Bethge, (2015). A Neural Algorithm of Artistic Style (https://arxiv.org/abs/1508.06576)
# Harish Narayanan, Convolutional neural networks for artistic style transfer. https://harishnarayanan.org/writing/artistic-style-transfer/
# Log0, TensorFlow Implementation of "A Neural Algorithm of Artistic Style". http://www.chioka.in/tensorflow-implementation-neural-algorithm-of-artistic-style
# Karen Simonyan and Andrew Zisserman (2015). Very deep convolutional networks for large-scale image recognition (https://arxiv.org/pdf/1409.1556.pdf)
# MatConvNet. http://www.vlfeat.org/matconvnet/pretrained/

import os
import traceback
from BackendJobModel import BackendJobModel
from BackendJobStatusModel import BackendJobStatusModel
import io
import base64
import sys
import time
import scipy.io
import scipy.misc
from PIL import Image
from nst_utils import *
import numpy as np
import tensorflow as tf

from flask import Flask, request, abort
from flask_restful import Resource, Api
import json
from flask_jsonpify import jsonify
from threading import Thread


init_completed = False
current_job = None
current_result = BackendJobStatusModel()
model = None
cancellation_requested = False


def compute_content_cost(a_C, a_G):
    """
    Computes the content cost
    
    Arguments:
    a_C -- tensor of dimension (1, n_H, n_W, n_C), hidden layer activations representing content of the image C 
    a_G -- tensor of dimension (1, n_H, n_W, n_C), hidden layer activations representing content of the image G
    
    Returns: 
    J_content -- scalar that you compute using equation 1 above.
    """
    
    # Retrieve dimensions from a_G
    m, n_H, n_W, n_C = a_G.get_shape().as_list()
    
    # Reshape a_C and a_G
    a_C_unrolled = tf.reshape(a_C, (m, n_H * n_W, n_C))
    a_G_unrolled = tf.reshape(a_G, (m, n_H * n_W, n_C))
    
    # compute the cost with tensorflow
    J_content = 1 / (4 * n_H * n_W * n_C) * tf.reduce_sum(tf.pow(a_C_unrolled - a_G_unrolled, 2))
    
    return J_content


def gram_matrix(A):
    """
    Argument:
    A -- matrix of shape (n_C, n_H*n_W)
    
    Returns:
    GA -- Gram matrix of A, of shape (n_C, n_C)
    """
    
    GA = tf.matmul(A, tf.transpose(A))
    
    return GA


def compute_layer_style_cost(a_S, a_G):
    """
    Arguments:
    a_S -- tensor of dimension (1, n_H, n_W, n_C), hidden layer activations representing style of the image S 
    a_G -- tensor of dimension (1, n_H, n_W, n_C), hidden layer activations representing style of the image G
    
    Returns: 
    J_style_layer -- tensor representing a scalar value, style cost defined above by equation (2)
    """
    
    # Retrieve dimensions from a_G
    m, n_H, n_W, n_C = a_G.get_shape().as_list()
    
    # Reshape the images to have them of shape (n_C, n_H*n_W)
    a_S = tf.reshape(tf.transpose(a_S, perm=[0,3,1,2]), (n_C, n_H*n_W))
    a_G = tf.reshape(tf.transpose(a_G, perm=[0,3,1,2]), (n_C, n_H*n_W))

    # Computing gram_matrices for both images S and G
    GS = gram_matrix(a_S)
    GG = gram_matrix(a_G)

    # Computing the loss
    J_style_layer = 1 / (4 * (n_C ** 2) * ((n_H*n_W) ** 2) ) * tf.reduce_sum(tf.square(tf.subtract(GS, GG)))
    
    return J_style_layer


STYLE_LAYERS = [
    ('conv1_1', 0.2),
    ('conv2_1', 0.2),
    ('conv3_1', 0.2),
    ('conv4_1', 0.2),
    ('conv5_1', 0.2)]


def compute_style_cost(model, STYLE_LAYERS):
    """
    Computes the overall style cost from several chosen layers
    
    Arguments:
    model -- our tensorflow model
    STYLE_LAYERS -- A python list containing:
                        - the names of the layers we would like to extract style from
                        - a coefficient for each of them
    
    Returns: 
    J_style -- tensor representing a scalar value, style cost defined above by equation (2)
    """
    
    # initialize the overall style cost
    J_style = 0
    a_S_var = {}
    a_S_placeholder = {}
    
    for layer_name, coeff in STYLE_LAYERS:

        # Select the output tensor of the currently selected layer
        out = model[layer_name]

        # Set a_S to be the hidden layer activation from the layer we have selected, by running the session on out
        a_S_placeholder[layer_name] = tf.placeholder(dtype = 'float32', shape = out.shape)
        a_S_var[layer_name] = tf.Variable(a_S_placeholder[layer_name], trainable = False, dtype = 'float32')

        # Set a_G to be the hidden layer activation from same layer. Here, a_G references model[layer_name] 
        # and isn't evaluated yet. Later in the code, we'll assign the image G as the model input, so that
        # when we run the session, this will be the activations drawn from the appropriate layer, with G as input.
        a_G = out
        
        # Compute style_cost for the current layer
        J_style_layer = compute_layer_style_cost(a_S_var[layer_name], a_G)

        # Add coeff * J_style_layer of this layer to overall style cost
        J_style += coeff * J_style_layer

    return J_style, a_S_var, a_S_placeholder


def total_cost(J_content, J_style, J_style2, alpha = 10, beta = 40, style2_weigth = 0):
    """
    Computes the total cost function
    
    Arguments:
    J_content -- content cost coded above
    J_style -- style cost coded above
    alpha -- hyperparameter weighting the importance of the content cost
    beta -- hyperparameter weighting the importance of the style cost
    
    Returns:
    J -- total cost as defined by the formula above.
    """
    
    J = alpha * J_content + beta * (J_style + style2_weigth * J_style2)
    
    return J


def transfer(session : tf.Session, model, input_placeholder, out, a_C_var, a_C_placeholder, a_S_var, a_S_placeholder, a_S2_var, a_S2_placeholder,
             J_content, J_style, J, content_cost_weight_var, style_cost_weight_var, style2_cost_weight_var,
             content_cost_weight_placeholder, style_cost_weight_placeholder, style2_cost_weight_placeholder,
             optimizer, optimizer_variables_initializer, train_step, num_iterations, content_image, style_image, style2_image, content_cost_weight, style_cost_weight):
    global current_result
    global cancellation_requested

    if cancellation_requested:
        return
        
    result_image = generate_noise_image(content_image)
        
    # Assign the content image to be the input of the VGG model.
    session.run(model['input'].initializer, feed_dict = {input_placeholder: content_image})

    # Set a_C to be the hidden layer activation from the layer we have selected
    a_C = session.run(out)
        
    # Assign the input of the model to be the "style" image
    session.run(model['input'].initializer, feed_dict = {input_placeholder: style_image})

    a_S = {}
    for layer_name, _ in STYLE_LAYERS:
        # Select the output tensor of the currently selected layer
        current_out = model[layer_name]

        # Set a_S to be the hidden layer activation from the layer we have selected, by running the session on current_out
        a_S[layer_name] = session.run(current_out)

    a_S2 = {}        
    if style2_image is not None:
        # Assign the input of the model to be the "style2" image    
        session.run(model['input'].initializer, feed_dict = {input_placeholder: style2_image})

        for layer_name, _ in STYLE_LAYERS:
            # Select the output tensor of the currently selected layer
            current_out = model[layer_name]

            # Set a_S to be the hidden layer activation from the layer we have selected, by running the session on current_out
            a_S2[layer_name] = session.run(current_out)

    session.run(a_C_var.initializer, feed_dict = {a_C_placeholder: a_C})

    for layer_name, _ in STYLE_LAYERS:
        session.run(a_S_var[layer_name].initializer, feed_dict = {a_S_placeholder[layer_name]: a_S[layer_name]})

    # Run the noisy input image (initial generated image) through the model.
    session.run(model['input'].initializer, feed_dict = {input_placeholder: result_image})

    session.run(content_cost_weight_var.initializer, feed_dict = {content_cost_weight_placeholder: content_cost_weight})

    session.run(style_cost_weight_var.initializer, feed_dict = {style_cost_weight_placeholder: style_cost_weight})

    if style2_image is not None:
        session.run(style2_cost_weight_var.initializer, feed_dict = {style2_cost_weight_placeholder: 1})
        for layer_name, _ in STYLE_LAYERS:
            session.run(a_S2_var[layer_name].initializer, feed_dict = {a_S2_placeholder[layer_name]: a_S2[layer_name]})
    else:
        session.run(style2_cost_weight_var.initializer, feed_dict = {style2_cost_weight_placeholder: 0})
        for layer_name, _ in STYLE_LAYERS:
            session.run(a_S2_var[layer_name].initializer, feed_dict = {a_S2_placeholder[layer_name]: np.zeros(model[layer_name].shape)})

    session.run(optimizer_variables_initializer)
    current_result.initializing = False
    last_update_time = time.time()
    for i in range(num_iterations):
        print('iteration={0}'.format(i))
        if cancellation_requested:
            return
    
        # Run the session on the train_step to minimize the total cost
        session.run(train_step)
        
        # Compute the generated image by running the session on the current model['input']
        generated_image = session.run(model['input'])

        if cancellation_requested:
            return
    
        time_elapsed_since_last_update = time.time() - last_update_time
        if time_elapsed_since_last_update > 0.5 or i == num_iterations - 1:
            last_update_time = time.time()
            Jt, Jc, Js = session.run([J, J_content, J_style])
            current_result.iteration = i
            current_result.totalCost = float(Jt)
            current_result.contentCost = float(Jc)
            current_result.styleCost = float(Js)
            current_result.resultImage = array_to_base64_data_url(generated_image)


def background():
    global init_completed
    global current_job
    global cancellation_requested

    model = None

    # Start the session
    with tf.Session() as session:

        while True:
            if not init_completed:

                (model, input_placeholder, out, a_C_var, a_C_placeholder, a_S_var, a_S_placeholder, s_S2_var, a_S2_placeholder,                   
                    J_content, J_style, J, 
                    content_cost_weight_var, style_cost_weight_var, style2_cost_weight_var,
                    content_cost_weight_placeholder, style_cost_weight_placeholder, style2_cost_weight_placeholder,
                    optimizer, optimizer_variables_initializer, train_step) = init()
                init_completed = True

            if current_job is not None:

                try:
                    content_base64 = get_base64_content(current_job.contentImage)
                    content_bytes = base64.b64decode(content_base64)
                    content_image_pil = Image.open(io.BytesIO(content_bytes))
                    content_image_original = np.array(content_image_pil)
                    content_image = reshape_and_normalize_image(content_image_original)

                    style_base64 = get_base64_content(current_job.styleImage)
                    style_bytes = base64.b64decode(style_base64)
                    style_image_pil = Image.open(io.BytesIO(style_bytes))
                    style_image_original = np.array(style_image_pil)
                    style_image = reshape_and_normalize_image(style_image_original)

                    style2_image = None
                    if current_job.style2Image is not None:
                        style2_base64 = get_base64_content(current_job.style2Image)
                        style2_bytes = base64.b64decode(style2_base64)
                        style2_image_pil = Image.open(io.BytesIO(style2_bytes))
                        style2_image_original = np.array(style2_image_pil)
                        style2_image = reshape_and_normalize_image(style2_image_original)
                    
                    transfer(session, model, input_placeholder, out, a_C_var, a_C_placeholder, a_S_var, a_S_placeholder, s_S2_var, a_S2_placeholder,
                             J_content, J_style, J, content_cost_weight_var, style_cost_weight_var, style2_cost_weight_var,
                             content_cost_weight_placeholder, style_cost_weight_placeholder, style2_cost_weight_placeholder,
                             optimizer, optimizer_variables_initializer, train_step, current_job.iterations, content_image, style_image, style2_image, current_job.contentCostWeight, current_job.styleCostWeight)

                    if cancellation_requested:
                        current_result.cancelled = True
                        cancellation_requested = False
                    else:
                        current_result.completed = True                        

                except:
                    print(traceback.format_exc())
                    current_result.failed = True
                
                current_job = None

            time.sleep(1)


class NeuralStyleTransferJobBackend(Resource):
    def get(self, id):
        global current_result

        if current_result.id != id:
            abort(404)

        return current_result.serialize()

    @convert_input_to(BackendJobModel)
    def put(self, job):
        global init_completed
        global current_job
        global current_result
        global cancellation_requested

        if not init_completed:
            return { 'created': False }

        current_result = BackendJobStatusModel()
        current_result.id = job.id
        
        while cancellation_requested:
            time.sleep(1)
                
        current_job = job
        
        return { 'created': True }

    def delete(self, id):
        global cancellation_requested
    
        cancellation_requested = True


def init():
    model, input_placeholder = load_vgg_model("pretrained-model/imagenet-vgg-verydeep-19.mat")
    print("Model weights have been loaded.")

    # Select the output tensor of layer conv4_2
    out = model['conv4_2']

    a_C_placeholder = tf.placeholder(dtype = 'float32', shape=out.shape)
    a_C_var = tf.Variable(a_C_placeholder, trainable = False, dtype = 'float32')

    # Set a_G to be the hidden layer activation from same layer. Here, a_G references model['conv4_2'] 
    # and isn't evaluated yet. Later in the code, we'll assign the image G as the model input, so that
    # when we run the session, this will be the activations drawn from the appropriate layer, with G as input.
    a_G = out
        
    # Compute the content cost
    J_content = compute_content_cost(a_C_var, a_G)
        
    # Compute the style cost
    J_style, a_S_var, a_S_placeholder = compute_style_cost(model, STYLE_LAYERS)
    J_style2, a_S2_var, a_S2_placeholder = compute_style_cost(model, STYLE_LAYERS)

    content_cost_weight_placeholder = tf.placeholder(dtype = 'float32', shape=())
    style_cost_weight_placeholder = tf.placeholder(dtype = 'float32', shape=())
    style2_cost_weight_placeholder = tf.placeholder(dtype = 'float32', shape=())
    content_cost_weight_var = tf.Variable(content_cost_weight_placeholder, trainable = False, dtype = 'float32')
    style_cost_weight_var = tf.Variable(style_cost_weight_placeholder, trainable = False, dtype = 'float32')
    style2_cost_weight_var = tf.Variable(style2_cost_weight_placeholder, trainable = False, dtype = 'float32')

    J = total_cost(J_content, J_style, J_style2, alpha=content_cost_weight_var, beta=style_cost_weight_var,
                   style2_weigth = style2_cost_weight_var)

    optimizer = tf.train.AdamOptimizer(2.0)
    train_step = optimizer.minimize(J)

    optimizer_variables_initializer = tf.variables_initializer(optimizer.variables())

    tf.get_default_graph().finalize()

    print('Computation graph has been initialized.')

    return (model, input_placeholder, out, a_C_var, a_C_placeholder, a_S_var, a_S_placeholder, a_S2_var, a_S2_placeholder,
           J_content, J_style, J, content_cost_weight_var, style_cost_weight_var, style2_cost_weight_var,
           content_cost_weight_placeholder, style_cost_weight_placeholder, style2_cost_weight_placeholder,
           optimizer, optimizer_variables_initializer, train_step)


def main(): 
    app = Flask(__name__)
    api = Api(app)
    api.add_resource(NeuralStyleTransferJobBackend, '/api/neural-style-transfer-backend/job', '/api/neural-style-transfer-backend/job/<id>')

    background_thread = Thread(target = background)
    background_thread.daemon = True
    background_thread.start()

    port = int(os.getenv('NST_BACKEND_PORT', '51286'))
    app.run(host="0.0.0.0", port=port)


if __name__ == "__main__":
    main()
